﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Common
{
    public enum OrdinalDirection
    {
        North,
        NorthEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West,
        NorthWest
    }
    public abstract class OrdinalWalker<TLocation> : Walker<OrdinalDirection, TLocation>
    {
        public override IEnumerable<OrdinalDirection> Values
        {
            get
            {
                return Enum.GetValues(typeof(OrdinalDirection)).Cast<OrdinalDirection>();
            }
        }

        public override OrdinalDirection GetOpposite(OrdinalDirection direction)
        {
            switch(direction)
            {
                case OrdinalDirection.North:
                    return OrdinalDirection.South;
                case OrdinalDirection.NorthEast:
                    return OrdinalDirection.SouthWest;
                case OrdinalDirection.East:
                    return OrdinalDirection.West;
                case OrdinalDirection.SouthEast:
                    return OrdinalDirection.NorthWest;
                case OrdinalDirection.South:
                    return OrdinalDirection.North;
                case OrdinalDirection.SouthWest:
                    return OrdinalDirection.NorthEast;
                case OrdinalDirection.West:
                    return OrdinalDirection.East;
                case OrdinalDirection.NorthWest:
                    return OrdinalDirection.SouthEast;
                default:
                    throw new InvalidOperationException("Called GetOpposite with a value that is out of range for the direction type.");
            }
        }
    }
}
