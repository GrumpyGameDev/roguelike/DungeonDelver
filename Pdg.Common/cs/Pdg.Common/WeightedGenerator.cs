﻿using System;
using System.Collections.Generic;

namespace Pdg.Common
{
    public interface IWeightedGenerator<TValue>
    {
        void SetWeight(TValue value, int weight);
        int GetWeight(TValue value);
        TValue Generate(Func<int, int> randomizer);
        IWeightedGenerator<TValue> Combine(IWeightedGenerator<TValue> other, Func<TValue, TValue, TValue> combiner);
        IEnumerable<TValue> Values { get; }
    }
    public class WeightedGenerator<TValue> : IWeightedGenerator<TValue>
    {
        private Dictionary<TValue, int> _table = new Dictionary<TValue, int>();
        private int _total = 0;

        public IEnumerable<TValue> Values
        {
            get
            {
                return _table.Keys;
            }
        }

        public IWeightedGenerator<TValue> Combine(IWeightedGenerator<TValue> other, Func<TValue, TValue, TValue> combiner)
        {
            IWeightedGenerator<TValue> result = new WeightedGenerator<TValue>();

            foreach(var outer in Values)
            {
                foreach(var inner in other.Values)
                {
                    var combined = combiner(outer, inner);
                    result.SetWeight(combined, result.GetWeight(combined) + GetWeight(outer) * other.GetWeight(inner));
                }
            }

            return result;
        }

        public TValue Generate(Func<int, int> randomizer)
        {
            int generated = randomizer(_total);
            foreach(var key in _table.Keys)
            {
                if(generated<_table[key])
                {
                    return key;
                }
                else
                {
                    generated -= _table[key];
                }
            }
            throw new InvalidOperationException("Error generating value from Weighted Generator. Likely the generator was empty, or the randomizer generated too high of a value.");
        }

        public int GetWeight(TValue value)
        {
            int result;
            if(_table.TryGetValue(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        public void SetWeight(TValue value, int weight)
        {
            _total -= GetWeight(value);
            if(weight>0)
            {
                if (_table.ContainsKey(value))
                {
                    _table[value] = weight;
                }
                else
                {
                    _table.Add(value, weight);
                }
            }
            else
            {
                if(_table.ContainsKey(value))
                {
                    _table.Remove(value);
                }
            }
            _total += GetWeight(value);
        }
    }
}
