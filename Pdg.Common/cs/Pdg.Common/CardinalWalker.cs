﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Common
{
    public enum CardinalDirection
    {
        North,
        East,
        South,
        West
    }
    public abstract class CardinalWalker<TLocation> : Walker<CardinalDirection, TLocation>
    {
        public override IEnumerable<CardinalDirection> Values
        {
            get
            {
                return Enum.GetValues(typeof(CardinalDirection)).Cast<CardinalDirection>();
            }
        }

        public override CardinalDirection GetOpposite(CardinalDirection direction)
        {
            switch(direction)
            {
                case CardinalDirection.North:
                    return CardinalDirection.South;
                case CardinalDirection.East:
                    return CardinalDirection.West;
                case CardinalDirection.South:
                    return CardinalDirection.North;
                case CardinalDirection.West:
                    return CardinalDirection.East;
                default:
                    throw new InvalidOperationException("Called GetOpposite with a value that is out of range for the direction type.");
            }
        }
    }
}
