﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Common
{
    public struct XY
    {
        public int X;
        public int Y;
        public int Distance2(XY other)
        {
            return (X - other.X) * (X - other.X) + (Y - other.Y) * (Y - other.Y);
        }
        public XY(int x,int y)
        {
            X = x;
            Y = y;
        }
        public static XY Create(int x, int y)
        {
            return new XY(x, y);
        }
        public static XY Zero
        {
            get
            {
                return Create(0, 0);
            }
        }
    }
}
