﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Sdl2
{
    public class QuitEvent : Event
    {
        internal QuitEvent()
            : base(EventType.Quit)
        {

        }
    }
}
