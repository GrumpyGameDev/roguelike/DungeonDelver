﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Sdl2
{
    public struct RendererFlags
    {
        public bool Software;
        public bool Accelerated;
        public bool PresentVSync;
        public bool TargetTexture;
    }
    public class Renderer: IDisposable
    {
        private IntPtr _ptr = IntPtr.Zero;

        internal IntPtr Pointer
        {
            get
            {
                return _ptr;
            }
        }

        private Renderer() { }

        public Renderer(Window window, int? index, RendererFlags flags)
        {
            _ptr = SDL2.SDL.SDL_CreateRenderer(window.Pointer, index ?? -1,
                ((flags.Software) ? (SDL2.SDL.SDL_RendererFlags.SDL_RENDERER_SOFTWARE) : (0)) |
                ((flags.Accelerated) ? (SDL2.SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED) : (0)) |
                ((flags.PresentVSync) ? (SDL2.SDL.SDL_RendererFlags.SDL_RENDERER_PRESENTVSYNC) : (0)) |
                ((flags.TargetTexture) ? (SDL2.SDL.SDL_RendererFlags.SDL_RENDERER_TARGETTEXTURE) : (0)));
        }

        public void FillRectangle(Rectangle? rectangle)
        {
            if(!rectangle.HasValue)
            {
                SDL2.SDL.SDL_RenderFillRect(_ptr, IntPtr.Zero);
            }
            else
            {
                SDL2.SDL.SDL_Rect rect = new SDL2.SDL.SDL_Rect() { x = rectangle.Value.X, y = rectangle.Value.Y, w = rectangle.Value.Width, h = rectangle.Value.Height };
                SDL2.SDL.SDL_RenderFillRect(_ptr, ref rect);
            }
        }

        public Color DrawColor
        {
            get
            {
                byte r, g, b, a;
                SDL2.SDL.SDL_GetRenderDrawColor(_ptr, out r, out g, out b, out a);
                return new Color(r, g, b, a);
            }
            set
            {
                SDL2.SDL.SDL_SetRenderDrawColor(_ptr, value.Red, value.Green, value.Blue, value.Alpha);
            }
        }

        public void Clear()
        {
            SDL2.SDL.SDL_RenderClear(_ptr);
        }

        public void Present()
        {
            SDL2.SDL.SDL_RenderPresent(_ptr);
        }

        public Rectangle LogicalSize
        {
            set
            {
                SDL2.SDL.SDL_RenderSetLogicalSize(_ptr, value.Width, value.Height);
            }
            get
            {
                int w, h;
                SDL2.SDL.SDL_RenderGetLogicalSize(_ptr, out w, out h);
                return new Rectangle() { Width = w, Height = h };
            }
        }

        public void Copy(Rectangle? destination, Texture texture, Rectangle? source)
        {
            if(destination.HasValue && source.HasValue)
            {
                SDL2.SDL.SDL_Rect src = source.Value.ToSDL_Rect;
                SDL2.SDL.SDL_Rect dst = destination.Value.ToSDL_Rect;
                SDL2.SDL.SDL_RenderCopy(_ptr, texture.Pointer, ref src, ref dst);
            }
            else if(destination.HasValue)
            {
                SDL2.SDL.SDL_Rect dst = destination.Value.ToSDL_Rect;
                SDL2.SDL.SDL_RenderCopy(_ptr, texture.Pointer, IntPtr.Zero, ref dst);
            }
            else if(source.HasValue)
            {
                SDL2.SDL.SDL_Rect src = source.Value.ToSDL_Rect;
                SDL2.SDL.SDL_RenderCopy(_ptr, texture.Pointer, ref src, IntPtr.Zero);
            }
            else
            {
                SDL2.SDL.SDL_RenderCopy(_ptr, texture.Pointer, IntPtr.Zero, IntPtr.Zero);
            }
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                if(_ptr!=IntPtr.Zero)
                {
                    SDL2.SDL.SDL_DestroyRenderer(_ptr);
                    _ptr = IntPtr.Zero;
                }

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~Renderer()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
