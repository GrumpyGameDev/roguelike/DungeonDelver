﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Sdl2
{
    public class Event
    {
        private Event() { }
        internal Event(EventType type)
        {
            Type = type;
        }
        public EventType Type { get; private set; }
        private static Event ConvertFromSDL_Event(SDL2.SDL.SDL_Event result)
        {
            switch (result.type)
            {
                case SDL2.SDL.SDL_EventType.SDL_QUIT:
                    return new QuitEvent();
                case SDL2.SDL.SDL_EventType.SDL_KEYDOWN:
                case SDL2.SDL.SDL_EventType.SDL_KEYUP:
                    return new KeyEvent(
                        result.type == SDL2.SDL.SDL_EventType.SDL_KEYDOWN ? EventType.KeyDown : EventType.KeyUp,
                        result.key.timestamp,
                        result.key.windowID,
                        result.key.state == SDL2.SDL.SDL_PRESSED,
                        result.key.repeat != 0,
                        (int)result.key.keysym.scancode,
                        (KeyCode)result.key.keysym.sym,
                        (ushort)result.key.keysym.mod);
                default:
                    return new Sdl2.Event(EventType.User);
            }
        }
        public static Event Wait()
        {
            SDL2.SDL.SDL_Event result;
            if(0==SDL2.SDL.SDL_WaitEvent(out result))
            {
                return null;
            }
            else
            {
                return ConvertFromSDL_Event(result);
            }
        }
        public static Event Poll()
        {
            SDL2.SDL.SDL_Event result;
            if (0 == SDL2.SDL.SDL_PollEvent(out result))
            {
                return null;
            }
            else
            {
                return ConvertFromSDL_Event(result);
            }
        }
    }
}
