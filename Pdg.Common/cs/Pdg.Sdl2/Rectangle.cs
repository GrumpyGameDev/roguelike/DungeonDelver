﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Sdl2
{
    public struct Rectangle
    {
        public int X;
        public int Y;
        public int Width;
        public int Height;
        public Rectangle(int x,int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }
        internal Rectangle(SDL2.SDL.SDL_Rect other)
            : this(other.x, other.y, other.w, other.h)
        {

        }
        internal SDL2.SDL.SDL_Rect ToSDL_Rect
        {
            get
            {
                return new SDL2.SDL.SDL_Rect() { x = X, y = Y, w = Width, h = Height };
            }
        }

    }
}
