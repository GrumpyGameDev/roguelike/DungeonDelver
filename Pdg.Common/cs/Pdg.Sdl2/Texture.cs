﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Sdl2
{
    public enum TextureAccess
    {
        Static,
        Streaming,
        Target
    }
    public class Texture : IDisposable
    {
        private IntPtr _ptr = IntPtr.Zero;

        internal IntPtr Pointer
        {
            get
            {
                return _ptr;
            }
        }
        public Texture() { }

        public Texture(Renderer renderer, string fileName)
        {
            using (Surface surface = new Surface(fileName))
            {
                _ptr = SDL2.SDL.SDL_CreateTextureFromSurface(renderer.Pointer, surface.Pointer);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                if(_ptr!=IntPtr.Zero)
                {
                    SDL2.SDL.SDL_DestroyTexture(_ptr);
                    _ptr = IntPtr.Zero;
                }

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~Texture()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion


    }
}
