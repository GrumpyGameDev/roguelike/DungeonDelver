﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pdg.Common;

namespace Pdg.Splorr.DungeonDelver
{
    public class ItemManager<TGame> : Manager<ItemType, ItemDescriptor<TGame>> where TGame:IGame<TGame>
    {
        internal static ItemDescriptor<TGame> Factory(ItemType type)
        {
            Func<ItemInstance<TGame>, byte> keyCharacter = i =>
            {
                switch (i.KeyColor)
                {
                    case KeyColor.Green:
                        return 0x86;
                    case KeyColor.Yellow:
                        return 0x96;
                    case KeyColor.Blue:
                        return 0xA6;
                    case KeyColor.Red:
                        return 0xB6;
                    case KeyColor.White:
                        return 0xC6;
                    case KeyColor.Cyan:
                        return 0xD6;
                    case KeyColor.Magenta:
                        return 0xE6;
                    case KeyColor.Orange:
                        return 0xF6;
                    default:
                        throw new NotImplementedException();
                }
            };
            ItemDescriptor<TGame> result = new ItemDescriptor<TGame>();
            result.ItemType = type;
            switch (type)
            {
                case ItemType.Key:
                    result.GetCharacter = keyCharacter;
                    result.Instantiate = (c) => 
                    {
                        ItemInstance<TGame> instance = new ItemInstance<TGame>(c);
                        instance.ItemType = ItemType.Key;
                        return instance;
                    };
                    result.CanPickUp = (c, i) => c.CreatureType == CreatureType.Tagon;
                    result.OnPickUp = (c,i) => 
                    {
                        if(c.KeyInventory.ContainsKey(i.KeyColor.Value))
                        {
                            c.KeyInventory[i.KeyColor.Value]++;
                        }
                        else
                        {
                            c.KeyInventory.Add(i.KeyColor.Value, 1);
                        }
                    };
                    break;
                case ItemType.Quest:
                    result.GetCharacter = x => 81;
                    result.Instantiate = c => {
                        ItemInstance<TGame> instance = new ItemInstance<TGame>(c);
                        instance.ItemType = ItemType.Quest;
                        return instance;
                    };
                    result.CanPickUp = (c, i) => c.CreatureType == CreatureType.Tagon;
                    result.OnPickUp = (c, i) =>
                    {
                        c.QuestItems++;
                    };
                    result.SpawnCount = 16;
                    break;
                case ItemType.SmallGold:
                    result.GetCharacter = x => 36;
                    result.Instantiate = c => {
                        ItemInstance<TGame> instance = new ItemInstance<TGame>(c);
                        instance.ItemType = ItemType.SmallGold;
                        instance.Gold = 25;
                        return instance;
                    };
                    result.CanPickUp = (c, i) => c.CreatureType == CreatureType.Tagon;
                    result.OnPickUp = (c, i) =>
                    {
                        c.Gold += i.Gold;
                    };
                    result.SpawnCount = 100;
                    break;
                case ItemType.MediumGold:
                    result.GetCharacter = x => 36;
                    result.Instantiate = c => {
                        ItemInstance<TGame> instance = new ItemInstance<TGame>(c);
                        instance.ItemType = ItemType.MediumGold;
                        instance.Gold = 50;
                        return instance;
                    };
                    result.CanPickUp = (c, i) => c.CreatureType == CreatureType.Tagon;
                    result.OnPickUp = (c, i) =>
                    {
                        c.Gold += i.Gold;
                    };
                    result.SpawnCount = 50;
                    break;
                case ItemType.BigGold:
                    result.GetCharacter = x => 36;
                    result.Instantiate = c => {
                        ItemInstance<TGame> instance = new ItemInstance<TGame>(c);
                        instance.ItemType = ItemType.BigGold;
                        instance.Gold = 100;
                        return instance;
                    };
                    result.CanPickUp = (c, i) => c.CreatureType == CreatureType.Tagon;
                    result.OnPickUp = (c, i) =>
                    {
                        c.Gold += i.Gold;
                    };
                    result.SpawnCount = 25;
                    break;
                case ItemType.Torch:
                    result.GetCharacter = x => 33;
                    result.Instantiate = c => {
                        ItemInstance<TGame> instance = new ItemInstance<TGame>(c);
                        instance.ItemType = ItemType.Torch;
                        instance.DurabilityType = DurabilityType.Torch;
                        instance.Durability = 100;
                        return instance;
                    };
                    result.CanPickUp = (c, i) => c.CreatureType == CreatureType.Tagon;
                    result.OnPickUp = (c, i) =>
                    {
                        if(c.Durabilities.ContainsKey(i.DurabilityType.Value))
                        {
                            c.Durabilities[i.DurabilityType.Value] += i.Durability;
                        }
                        else
                        {
                            c.Durabilities.Add(i.DurabilityType.Value, i.Durability);
                        }
                        c.Descriptor.AutoEquip(c);
                    };
                    result.SpawnCount = 100;
                    break;
                case ItemType.Lantern:
                    result.GetCharacter = x => 35;
                    result.Instantiate = c => {
                        ItemInstance<TGame> instance = new ItemInstance<TGame>(c);
                        instance.ItemType = ItemType.Lantern;
                        instance.DurabilityType = DurabilityType.Lantern;
                        instance.Durability = 250;
                        return instance;
                    };
                    result.CanPickUp = (c, i) => c.CreatureType == CreatureType.Tagon;
                    result.OnPickUp = (c, i) =>
                    {
                        if (c.Durabilities.ContainsKey(i.DurabilityType.Value))
                        {
                            c.Durabilities[i.DurabilityType.Value] += i.Durability;
                        }
                        else
                        {
                            c.Durabilities.Add(i.DurabilityType.Value, i.Durability);
                        }
                        c.Descriptor.AutoEquip(c);
                    };
                    result.SpawnCount = 50;
                    break;
                case ItemType.MagicLantern:
                    result.GetCharacter = x => 99;
                    result.Instantiate = c => {
                        ItemInstance<TGame> instance = new ItemInstance<TGame>(c);
                        instance.ItemType = ItemType.MagicLantern;
                        instance.DurabilityType = DurabilityType.MagicLantern;
                        instance.Durability = 500;
                        return instance;
                    };
                    result.CanPickUp = (c, i) => c.CreatureType == CreatureType.Tagon;
                    result.OnPickUp = (c, i) =>
                    {
                        if (c.Durabilities.ContainsKey(i.DurabilityType.Value))
                        {
                            c.Durabilities[i.DurabilityType.Value] += i.Durability;
                        }
                        else
                        {
                            c.Durabilities.Add(i.DurabilityType.Value, i.Durability);
                        }
                        c.Descriptor.AutoEquip(c);
                    };
                    result.SpawnCount = 25;
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }
        public ItemManager() 
            : base(Factory, x => { })
        {
        }
    }
}
