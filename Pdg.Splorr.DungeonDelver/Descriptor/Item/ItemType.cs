﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Splorr.DungeonDelver
{
    public enum ItemType
    {
        Key,
        Quest,
        SmallGold,
        MediumGold,
        BigGold,
        Torch,
        Lantern,
        MagicLantern
    }
}
