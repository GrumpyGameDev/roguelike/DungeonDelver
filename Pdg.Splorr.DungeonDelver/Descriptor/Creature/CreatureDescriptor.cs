﻿using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Splorr.DungeonDelver
{
    public class CreatureDescriptor<TGame> where TGame:IGame<TGame>
    {
        internal CreatureType CreatureType
        {
            get; set;
        }
        internal int SpawnCount
        {
            get; set;
        }
        public Func<CreatureInstance<TGame>, byte> GetCharacter
        {
            get; set;
        }
        public Func<CreatureInstance<TGame>, double> GetMoveCost
        {
            get;set;
        }
        internal bool IsPlayer
        {
            get; set;
        }
        internal Func<MapCell<TGame>, CreatureInstance<TGame>> Instantiate
        {
            get;set;
        }
        internal Action<CreatureInstance<TGame>> AutoEquip
        {
            get;set;
        }
        internal Action<CreatureInstance<TGame>> PostMove
        {
            get;set;
        }
        public CreatureDescriptor() 
        {
        }
    }
}
