﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pdg.Common;

namespace Pdg.Splorr.DungeonDelver
{
    public class CreatureManager<TGame>: Manager<CreatureType, CreatureDescriptor<TGame>> where TGame : IGame<TGame>
    {
        internal static CreatureDescriptor<TGame> Factory(CreatureType type)
        {
            CreatureDescriptor<TGame> result = new CreatureDescriptor<TGame>();
            result.CreatureType = type;
            switch(type)
            {
                case CreatureType.Tagon:
                    result.GetCharacter = x=> 0;
                    result.SpawnCount = 1;
                    result.IsPlayer = true;
                    result.GetMoveCost = c => 1.0;
                    result.Instantiate = c =>
                    {
                        CreatureInstance<TGame> instance = new CreatureInstance<TGame>(c);
                        instance.CreatureType = CreatureType.Tagon;
                        instance.KeyInventory = new Dictionary<KeyColor, int>();
                        instance.Durabilities = new Dictionary<DurabilityType, int>();
                        return instance;
                    };
                    result.AutoEquip = c =>
                    {
                        //light
                        if(c.Durabilities.ContainsKey(DurabilityType.MagicLantern) && c.Durabilities[DurabilityType.MagicLantern]>0)
                        {
                            c.LightType = LightType.MagicLantern;
                        }
                        else if (c.Durabilities.ContainsKey(DurabilityType.Lantern) && c.Durabilities[DurabilityType.Lantern] > 0)
                        {
                            c.LightType = LightType.Lantern;
                        }
                        else if (c.Durabilities.ContainsKey(DurabilityType.Torch) && c.Durabilities[DurabilityType.Torch] > 0)
                        {
                            c.LightType = LightType.Torch;
                        }
                        else
                        {
                            c.LightType = null;
                        }
                    };
                    result.PostMove = c => 
                    {
                        //light
                        DurabilityType? durabilityType =
                            (c.LightType == LightType.MagicLantern) ? (DurabilityType.MagicLantern) :
                            (c.LightType == LightType.Lantern) ? (DurabilityType.Lantern) :
                            (c.LightType == LightType.Torch) ? (DurabilityType.Torch) :
                            ((DurabilityType?)null);
                        if(durabilityType.HasValue)
                        {
                            if(c.Durabilities.ContainsKey(durabilityType.Value))
                            {
                                c.Durabilities[durabilityType.Value]--;
                                if(c.Durabilities[durabilityType.Value]<=0)
                                {
                                    c.Durabilities[durabilityType.Value] = 0;
                                    c.LightType = null;
                                    c.Descriptor.AutoEquip(c);
                                }
                            }
                        }
                    };
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }
        public CreatureManager() 
            : base(Factory, x => { })
        {
        }
    }
}
