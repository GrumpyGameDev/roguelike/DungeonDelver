﻿using Newtonsoft.Json.Linq;
using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pdg.Splorr.DungeonDelver
{
    public interface IGame<TGame> where TGame:IGame<TGame>
    {
        TerrainManager<TGame> TerrainManager { get; }
        CreatureManager<TGame> CreatureManager { get;  }
        ItemManager<TGame> ItemManager { get; }
        IntGeneratorManager IntGeneratorManager { get; }
        int FrameCounter { get; }
    }
    public class Game:IGame<Game>
    {
        private class Properties
        {
            public const string Atlas = "atlas";
            public const string Avatar = "avatar";
            public const string Level = "level";
        }

        public TerrainManager<Game> TerrainManager { get; private set; }
        public CreatureManager<Game> CreatureManager { get; private set; }
        public ItemManager<Game> ItemManager { get; private set; }
        public IntGeneratorManager IntGeneratorManager { get; private set; }

        public JObject Export()
        {
            JObject export = new JObject();
            export[Properties.Level] = (int)Level;
            export[Properties.Avatar] = Avatar.Export();
            export[Properties.Atlas] = Atlas.Export();            
            return export;
        }

        public Avatar Avatar { get; set; }
        public Atlas<Game> Atlas { get; set; }
        public int FrameCounter { get; set; }

        internal DifficultyLevel Level { get; private set; }
        public Game(DifficultyLevel level)
        {
            Level = level;
            TerrainManager = new TerrainManager<Game>();
            ItemManager = new ItemManager<Game>();
            CreatureManager = new CreatureManager<Game>();
        }
        internal Game(JObject import)
        {
            TerrainManager = new TerrainManager<Game>();
            ItemManager = new ItemManager<Game>();
            CreatureManager = new CreatureManager<Game>();
            IntGeneratorManager = new IntGeneratorManager();
            Level = (DifficultyLevel)import[Properties.Level].ToObject<int>();
            Avatar = Avatar.Create(import[Properties.Avatar] as JObject);
            Atlas = new Atlas<Game>(this, import[Properties.Atlas] as JArray);
        }

        private static Maze<CardinalDirection, XY> GenerateMaze(Func<int,int> randomizer)
        {
            Maze<CardinalDirection, XY> maze = new Maze<CardinalDirection, XY>(x => x.X>=0 && x.Y>=0 && x.X<Atlas<Game>.Columns && x.Y<Atlas<Game>.Rows, new XYCardinalWalker());
            for (int x = Atlas<Game>.ColumnLeft; x <= Atlas<Game>.ColumnRight; ++x)
            {
                for (int y = Atlas<Game>.RowTop; y <= Atlas<Game>.RowBottom; ++y)
                {
                    maze[XY.Create(x, y)] = new MazeCell<CardinalDirection>();
                }
            }
            maze.Generate(1, randomizer);
            return maze;
        }

        public void Move(XY atlasPosition, XY mapPosition, CardinalDirection direction)
        {
            var cell = Atlas[atlasPosition][mapPosition];
            var creature = cell.Creature;
            bool addVisit = false;
            double moveCost = 0.0;
            cell.Creature = null;

            XYCardinalWalker walker = new XYCardinalWalker();
            var nextMapPosition = walker.Step(mapPosition, direction);
            if(Atlas[atlasPosition].Locations.Contains(nextMapPosition))
            {
                var nextCell = Atlas[atlasPosition][nextMapPosition];
                var terrain = nextCell.Terrain;
                var terrainDescriptor = TerrainManager[terrain.TerrainType];
                if (terrainDescriptor.CanEnter(terrain, creature))
                {
                    //check for item
                    moveCost = creature.Descriptor.GetMoveCost(creature);
                    if (nextCell.Item!=null)
                    {
                        if(nextCell.Item.Descriptor.CanPickUp(creature, nextCell.Item))
                        {
                            nextCell.Item.Descriptor.OnPickUp(creature, nextCell.Item);
                            nextCell.Item = null;
                        }
                        else
                        {
                            nextCell.Item.Descriptor.OnBump(creature, nextCell.Item);
                            nextCell = cell;
                            nextMapPosition = mapPosition;
                            moveCost = 0.0;
                        }
                    }

                    cell = nextCell;
                    if (creature.Descriptor.IsPlayer)
                    {
                        Avatar = Avatar.Create(Avatar.AtlasPosition, nextMapPosition, null);
                    }
                }
            }
            else
            {
                //move outside of map!
                var nextAtlasPosition = walker.Step(atlasPosition, direction);
                nextMapPosition = Map<Game>.Wrap(nextMapPosition);
                var nextCell = Atlas[nextAtlasPosition][nextMapPosition];
                var terrain = nextCell.Terrain;
                var terrainDescriptor = TerrainManager[terrain.TerrainType];
                if (terrainDescriptor.CanEnter(terrain, creature))
                {
                    //check for item
                    moveCost = creature.Descriptor.GetMoveCost(creature);
                    if (nextCell.Item != null)
                    {
                        if (nextCell.Item.Descriptor.CanPickUp(creature, nextCell.Item))
                        {
                            nextCell.Item.Descriptor.OnPickUp(creature, nextCell.Item);
                            nextCell.Item = null;
                        }
                        else
                        {
                            nextCell.Item.Descriptor.OnBump(creature, nextCell.Item);
                            nextCell = cell;
                            nextMapPosition = mapPosition;
                            nextAtlasPosition = atlasPosition;
                            moveCost = 0.0;
                        }
                    }

                    if (creature.Descriptor.IsPlayer)
                    {
                        addVisit = (Avatar.AtlasPosition.X != nextAtlasPosition.X || Avatar.AtlasPosition.Y != nextAtlasPosition.Y);
                    }

                    cell = nextCell;
                    if (creature.Descriptor.IsPlayer)
                    {
                        Avatar = Avatar.Create(nextAtlasPosition, nextMapPosition, null);
                    }
                }
            }

            cell.Creature = creature;
            creature.MapCell = cell;
            creature.TurnCounter += moveCost;
            creature.Descriptor.PostMove(creature);

            if (addVisit && creature.Descriptor.IsPlayer)
            {
                Atlas[Avatar.AtlasPosition].AddVisit();
            }
        }

        public void Reset()
        {
            Dictionary<int, IWeightedGenerator<bool>> chamberGenerators = new Dictionary<int, IWeightedGenerator<bool>>();

            chamberGenerators[1] = new WeightedGenerator<bool>();
            chamberGenerators[1].SetWeight(true, 1);

            chamberGenerators[2] = new WeightedGenerator<bool>();
            chamberGenerators[2].SetWeight(true, 1);
            chamberGenerators[2].SetWeight(false, 1);

            chamberGenerators[3] = new WeightedGenerator<bool>();
            chamberGenerators[3].SetWeight(true, 3);
            chamberGenerators[3].SetWeight(false, 1);

            chamberGenerators[4] = new WeightedGenerator<bool>();
            chamberGenerators[4].SetWeight(true, 1);
            chamberGenerators[4].SetWeight(false, 3);

            IWeightedGenerator<KeyColor> keyGenerator = new WeightedGenerator<KeyColor>();
            keyGenerator.SetWeight(KeyColor.Green, 128);
            keyGenerator.SetWeight(KeyColor.Yellow, 64);
            keyGenerator.SetWeight(KeyColor.Blue, 32);
            keyGenerator.SetWeight(KeyColor.Red, 16);
            keyGenerator.SetWeight(KeyColor.White, 8);
            keyGenerator.SetWeight(KeyColor.Cyan, 4);
            keyGenerator.SetWeight(KeyColor.Magenta, 2);
            keyGenerator.SetWeight(KeyColor.Orange, 1);

            Dictionary<KeyColor, int> keyCounters = new Dictionary<KeyColor, int>();

            Random random = new Random();
            var maze = GenerateMaze(x => random.Next(x));
            XYCardinalWalker walker = new XYCardinalWalker();
            Atlas = new Atlas<Game>(this);
            foreach(var atlasLocation in maze.Locations)
            {
                var map = new Map<Game>(Atlas);
                Atlas[atlasLocation] = map;
                foreach(var mapLocation in map.Locations)
                {
                    var mapCell = map[mapLocation];
                    var terrainInstance = TerrainManager[TerrainType.Wall].Instantiate(mapCell);
                    mapCell.Terrain = terrainInstance;
                }
                var mazeCell = maze[atlasLocation];
                if(mazeCell.HasExit(CardinalDirection.North))
                {
                    bool hasDoor = maze[walker.Step(atlasLocation, CardinalDirection.North)].Exits.Count() == 1;
                    KeyColor? doorType = hasDoor ? keyGenerator.Generate(x => random.Next(x)) : (KeyColor?)null;
                    for (int column=Map<Game>.CorridorLeft;column<=Map<Game>.CorridorRight;++column)
                    {
                        for(int row=Map<Game>.RowTop;row<=Map<Game>.CorridorBottom;++row)
                        {
                            var terrainInstance = new TerrainInstance<Game>(map[XY.Create(column, row)]);
                            terrainInstance.TerrainType = (row == Map<Game>.RowTop) ? (hasDoor ? TerrainType.NorthDoor : TerrainType.NorthEdge) : (TerrainType.Open);
                            terrainInstance.KeyColor = doorType;
                            map[XY.Create(column, row)].Terrain = terrainInstance;
                        }
                    }
                    if (doorType.HasValue)
                    {
                        if (keyCounters.ContainsKey(doorType.Value))
                        {
                            keyCounters[doorType.Value]++;
                        }
                        else
                        {
                            keyCounters.Add(doorType.Value, 1);
                        }
                    }
                }
                if (mazeCell.HasExit(CardinalDirection.East))
                {
                    bool hasDoor = maze[walker.Step(atlasLocation, CardinalDirection.East)].Exits.Count() == 1;
                    KeyColor? doorType = hasDoor ? keyGenerator.Generate(x => random.Next(x)) : (KeyColor?)null;
                    for (int column = Map<Game>.CorridorLeft; column <= Map<Game>.ColumnRight; ++column)
                    {
                        for (int row = Map<Game>.CorridorTop; row <= Map<Game>.CorridorBottom; ++row)
                        {
                            var terrainInstance = new TerrainInstance<Game>(map[XY.Create(column, row)]);
                            terrainInstance.TerrainType = (column == Map<Game>.ColumnRight) ? (hasDoor ? TerrainType.EastDoor : TerrainType.EastEdge) : (TerrainType.Open);
                            terrainInstance.KeyColor = doorType;
                            map[XY.Create(column, row)].Terrain = terrainInstance;
                        }
                    }
                    if(doorType.HasValue)
                    {
                        if(keyCounters.ContainsKey(doorType.Value))
                        {
                            keyCounters[doorType.Value]++;
                        }
                        else
                        {
                            keyCounters.Add(doorType.Value, 1);
                        }
                    }
                }
                if (mazeCell.HasExit(CardinalDirection.South))
                {
                    bool hasDoor = maze[walker.Step(atlasLocation, CardinalDirection.South)].Exits.Count() == 1;
                    KeyColor? doorType = hasDoor ? keyGenerator.Generate(x => random.Next(x)) : (KeyColor?)null;
                    for (int column = Map<Game>.CorridorLeft; column <= Map<Game>.CorridorRight; ++column)
                    {
                        for (int row = Map<Game>.CorridorTop; row <= Map<Game>.RowBottom; ++row)
                        {
                            var terrainInstance = new TerrainInstance<Game>(map[XY.Create(column, row)]);
                            terrainInstance.TerrainType = (row == Map<Game>.RowBottom) ? (hasDoor ? TerrainType.SouthDoor : TerrainType.SouthEdge) : (TerrainType.Open);
                            terrainInstance.KeyColor = doorType;
                            map[XY.Create(column, row)].Terrain = terrainInstance;
                        }
                    }
                    if (doorType.HasValue)
                    {
                        if (keyCounters.ContainsKey(doorType.Value))
                        {
                            keyCounters[doorType.Value]++;
                        }
                        else
                        {
                            keyCounters.Add(doorType.Value, 1);
                        }
                    }
                }
                if (mazeCell.HasExit(CardinalDirection.West))
                {
                    bool hasDoor = maze[walker.Step(atlasLocation, CardinalDirection.West)].Exits.Count() == 1;
                    KeyColor? doorType = hasDoor ? keyGenerator.Generate(x => random.Next(x)) : (KeyColor?)null;
                    for (int column = Map<Game>.ColumnLeft; column <= Map<Game>.CorridorRight; ++column)
                    {
                        for (int row = Map<Game>.CorridorTop; row <= Map<Game>.CorridorBottom; ++row)
                        {
                            var terrainInstance = new TerrainInstance<Game>(map[XY.Create(column, row)]);
                            terrainInstance.TerrainType = (column == 0) ? (hasDoor ? TerrainType.WestDoor : TerrainType.WestEdge) : (TerrainType.Open);
                            terrainInstance.KeyColor = doorType;
                            map[XY.Create(column, row)].Terrain = terrainInstance;
                        }
                    }
                    if (doorType.HasValue)
                    {
                        if (keyCounters.ContainsKey(doorType.Value))
                        {
                            keyCounters[doorType.Value]++;
                        }
                        else
                        {
                            keyCounters.Add(doorType.Value, 1);
                        }
                    }
                }
                var terrainType = mazeCell.Exits.Count() == 1 ? TerrainType.OpenDeadEnd : TerrainType.Open;
                if(chamberGenerators[mazeCell.Exits.Count()].Generate(x=>random.Next(x)))
                {
                    for (int column = Map<Game>.ColumnLeft+1; column <= Map<Game>.ColumnRight-1; ++column)
                    {
                        for (int row = Map<Game>.RowTop+1; row <= Map<Game>.RowBottom-1; ++row)
                        {
                            var terrainInstance = TerrainManager[terrainType].Instantiate(map[XY.Create(column, row)]);
                            map[XY.Create(column, row)].Terrain = terrainInstance;
                        }
                    }
                }
            }
            foreach(CreatureType creatureType in Enum.GetValues(typeof(CreatureType)))
            {
                var creatureDescriptor = CreatureManager[creatureType];
                int spawnCount = creatureDescriptor.SpawnCount;
                while(spawnCount>0)
                {
                    XY atlasLocation;
                    XY mapLocation;
                    bool found = false;
                    CreatureInstance<Game> creature = CreatureManager[creatureType].Instantiate(null);
                    do
                    {
                        atlasLocation = XY.Create(random.Next(Atlas<Game>.Columns), random.Next(Atlas<Game>.Rows));
                        mapLocation = XY.Create(random.Next(Map<Game>.Columns), random.Next(Map<Game>.Rows));
                        var mapCell = Atlas[atlasLocation][mapLocation];
                        var terrain = mapCell.Terrain;
                        var terrainType = terrain.TerrainType;
                        found = mapCell.Creature==null && mapCell.Item == null && TerrainManager[terrainType].CanSpawn(terrain, creature);
                    } while (!found);
                    Atlas[atlasLocation][mapLocation].Creature = creature;
                    creature.MapCell = Atlas[atlasLocation][mapLocation];
                    if (CreatureManager[creatureType].IsPlayer)
                    {
                        Avatar = Avatar.Create(atlasLocation, mapLocation, null);
                    }
                    spawnCount--;
                }
            }
            //spawn keys
            foreach (KeyColor keyType in keyCounters.Keys)
            {
                int counter = keyCounters[keyType];
                while (counter > 0)
                {
                    XY atlasLocation;
                    XY mapLocation;
                    bool found = false;
                    ItemInstance<Game> item = ItemManager[ItemType.Key].Instantiate(null);
                    item.KeyColor = keyType;
                    do
                    {
                        atlasLocation = XY.Create(random.Next(Atlas<Game>.Columns), random.Next(Atlas<Game>.Rows));
                        mapLocation = XY.Create(random.Next(Map<Game>.Columns), random.Next(Map<Game>.Rows));
                        var mapCell = Atlas[atlasLocation][mapLocation];
                        var terrain = mapCell.Terrain;
                        var terrainType = terrain.TerrainType;
                        found = mapCell.Creature == null && mapCell.Item == null && TerrainManager[terrainType].CanPlace(terrain, item);
                    } while (!found);
                    Atlas[atlasLocation][mapLocation].Item = item;
                    item.MapCell = Atlas[atlasLocation][mapLocation];
                    counter--;
                }
            }
            //spawn items
            foreach(ItemType itemType in Enum.GetValues(typeof(ItemType)).Cast<ItemType>())
            {
                int counter = ItemManager[itemType].SpawnCount;
                while(counter>0)
                {
                    XY atlasLocation;
                    XY mapLocation;
                    bool found = false;
                    ItemInstance<Game> item = ItemManager[itemType].Instantiate(null);
                    do
                    {
                        atlasLocation = XY.Create(random.Next(Atlas<Game>.Columns), random.Next(Atlas<Game>.Rows));
                        mapLocation = XY.Create(random.Next(Map<Game>.Columns), random.Next(Map<Game>.Rows));
                        var mapCell = Atlas[atlasLocation][mapLocation];
                        var terrain = mapCell.Terrain;
                        var terrainType = terrain.TerrainType;
                        found = mapCell.Creature == null && mapCell.Item == null && TerrainManager[terrainType].CanPlace(terrain, item);
                    } while (!found);
                    Atlas[atlasLocation][mapLocation].Item = item;
                    item.MapCell = Atlas[atlasLocation][mapLocation];
                    counter--;
                }
            }
            Atlas[Avatar.AtlasPosition].AddVisit();
        }

        internal static Game Create(DifficultyLevel level)
        {
            return new Game(level);
        }
        internal static Game Import(JObject import)
        {
            return new Game(import);
        }
    }
}
