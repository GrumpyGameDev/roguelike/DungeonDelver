﻿using Newtonsoft.Json;
using Pdg.CoCo;
using Pdg.Sdl2;
using System;
using System.IO;
using System.Linq;

namespace Pdg.Splorr.DungeonDelver.SdlClient
{
    internal static class Play
    {
        private static void Redraw(Game game, TextScreen textScreen)
        {
            textScreen.Clear();
            var map = game.Atlas[game.Avatar.AtlasPosition];
            var creature = map[game.Avatar.MapPosition].Creature;
            int lightRadius =
                (creature.LightType == LightType.MagicLantern) ? (12) :
                (creature.LightType == LightType.Lantern) ? (9) :
                (creature.LightType == LightType.Torch) ? (6) :
                (3);
            foreach (var xy in map.Locations)
            {
                textScreen.CursorColumn = xy.X;
                textScreen.CursorRow = xy.Y;
                var cell = map[xy];
                if(xy.Distance2(game.Avatar.MapPosition)>=lightRadius*lightRadius)
                {
                    textScreen.Write(128, true);
                }
                else if (cell.Creature != null)
                {
                    textScreen.Write(cell.Creature.Descriptor.GetCharacter(cell.Creature), true);
                }
                else if (cell.Item != null)
                {
                    textScreen.Write(cell.Item.Descriptor.GetCharacter(cell.Item), true);
                }
                else
                {
                    textScreen.Write(cell.Terrain.Descriptor.GetCharacter(cell.Terrain), true);
                }
            }
            var player = game.Atlas[game.Avatar.AtlasPosition][game.Avatar.MapPosition].Creature;
            int row = 0;
            foreach(var keyColor in player.KeyInventory.Keys)
            {
                textScreen.CursorColumn = Map<Game>.Columns;
                textScreen.CursorRow = row;
                textScreen.Write((byte)(0x86 + (0x10 * (int)keyColor)), true);
                textScreen.Write(player.KeyInventory[keyColor].ToString(), true);
                row++;
            }
            textScreen.CursorColumn = Map<Game>.Columns;
            textScreen.CursorRow = row;
            textScreen.Write(string.Format("Q:{0}", player.QuestItems), true);
            row++;
            textScreen.CursorColumn = Map<Game>.Columns;
            textScreen.CursorRow = row;
            textScreen.Write(string.Format("${0}", player.Gold), true);
            row++;
            textScreen.CursorColumn = Map<Game>.Columns;
            textScreen.CursorRow = row;
            textScreen.Write(string.Format("V:{0}", game.Atlas[game.Avatar.AtlasPosition].Visits), true);
            row++;
        }
        internal static bool Run(Game game, Renderer renderer, TextScreen textScreen)
        {
            return Utility.StandardEventHandler(
                () =>
                {
                    Redraw(game,textScreen);
                    renderer.Clear();
                    textScreen.Render();
                    renderer.Present();
                    game.FrameCounter++;
                },
                false,
                (k, r) =>
                {
                    switch (k.KeyCode)
                    {
                        case KeyCode.Left:
                            game.Move(game.Avatar.AtlasPosition, game.Avatar.MapPosition, Common.CardinalDirection.West);
                            break;
                        case KeyCode.Right:
                            game.Move(game.Avatar.AtlasPosition, game.Avatar.MapPosition, Common.CardinalDirection.East);
                            break;
                        case KeyCode.Up:
                            game.Move(game.Avatar.AtlasPosition, game.Avatar.MapPosition, Common.CardinalDirection.North);
                            break;
                        case KeyCode.Down:
                            game.Move(game.Avatar.AtlasPosition, game.Avatar.MapPosition, Common.CardinalDirection.South);
                            break;
                        case KeyCode.Escape:
                            var save = QuerySave.Run(renderer, textScreen);
                            if(save.HasValue)
                            {
                                if(save.Value)
                                {
                                    var data = game.Export();
                                    using (StreamWriter file = File.CreateText("savegame.json"))
                                    using (JsonTextWriter writer = new JsonTextWriter(file))
                                    {
                                        data.WriteTo(writer);
                                    }
                                }
                                var abandon = ConfirmAbandon.Run(renderer, textScreen);
                                if (abandon.HasValue)
                                {
                                    if (abandon.Value)
                                    {
                                        r(true);
                                    }
                                }
                                else
                                {
                                    r(false);
                                }
                            }
                            else
                            {
                                r(false);
                            }
                            break;
                    }
                });
        }
    }
}
