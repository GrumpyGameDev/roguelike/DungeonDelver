﻿using Pdg.CoCo;
using Pdg.Sdl2;

namespace Pdg.Splorr.DungeonDelver.SdlClient
{
    internal static class About
    {
        private static void Redraw(TextScreen textScreen)
        {
            textScreen.Clear();
            textScreen.WriteLine("=ABOUT SPLORR!! DUNGEON DELVER==================================", false);
            textScreen.WriteLine("CREATED BY ERNEST PAZERA.", false);
            textScreen.WriteLine("", false);
            textScreen.WriteLine("WHILE I HOPE THAT OTHERS FIND   THIS GAME FUN, I WROTE IT FOR   ME.", false);
            textScreen.WriteLine("", false);
            textScreen.WriteLine("DEDICATED TO MY SON, OWEN.", false);
            textScreen.WriteLine("", false);
            textScreen.WriteLine("ERNESTPAZERA@GMAIL.COM", false);
            textScreen.CursorRow = 15;
            textScreen.Write("{esc} GO BACK", true);

        }
        internal static bool Run(Renderer renderer, TextScreen textScreen)
        {
            return Utility.StandardEventHandler(
                () =>
                {
                    Redraw(textScreen);
                    renderer.Clear();
                    textScreen.Render();
                    renderer.Present();
                },
                false,
                (k, r) =>
                {
                    switch (k.KeyCode)
                    {
                        case KeyCode.Escape:
                            r(true);
                            break;
                    }
                });
        }
    }
}
