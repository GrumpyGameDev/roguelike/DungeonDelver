﻿using Pdg.Sdl2;
using System;

namespace Pdg.Splorr.DungeonDelver.SdlClient
{
    internal static class Utility
    {
        internal static TResult StandardEventHandler<TResult>(Action renderAction, TResult quitResult, Action<KeyEvent, Action<TResult>> handler)
        {
            bool done = false;
            TResult result = default(TResult);
            Action<TResult> responder = x => 
            {
                result = x;
                done = true;
            };
            while(!done)
            {
                renderAction();
                Event evt = Event.Poll();
                if (evt != null)
                {
                    if (evt.Type == EventType.Quit)
                    {
                        return quitResult;
                    }
                    else if (evt.Type == EventType.KeyDown)
                    {
                        KeyEvent keyEvent = evt as KeyEvent;
                        handler(keyEvent, responder);
                    }
                }
            }
            return result;
        }
    }
}
