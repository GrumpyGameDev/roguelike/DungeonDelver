﻿using Pdg.CoCo;
using Pdg.Sdl2;

namespace Pdg.Splorr.DungeonDelver.SdlClient
{
    internal static class ChooseDifficulty
    {
        internal static DifficultyLevel? Run(Renderer renderer, TextScreen textScreen)
        {
            textScreen.Clear();
            textScreen.WriteLine("CHOOSE DIFFICULTY!", false);
            textScreen.WriteLine("", false);
            textScreen.WriteLine("{e}ASY", false);
            textScreen.WriteLine("{m}EDIUM", false);
            textScreen.WriteLine("{h}ARD", false);
            return Utility.StandardEventHandler<DifficultyLevel?>(
                () => 
                {
                    renderer.Clear();
                    textScreen.Render();
                    renderer.Present();
                }, 
                null, 
                (k, r) => 
                {
                    switch (k.KeyCode)
                    {
                        case KeyCode.E:
                            r(DifficultyLevel.Easy);
                            break;
                        case KeyCode.M:
                            r(DifficultyLevel.Medium);
                            break;
                        case KeyCode.H:
                            r(DifficultyLevel.Hard);
                            break;
                    }
                });
        }
    }
}
