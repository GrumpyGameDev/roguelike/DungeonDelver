﻿using Pdg.CoCo;
using Pdg.Sdl2;

namespace Pdg.Splorr.DungeonDelver.SdlClient
{
    class Program
    {
        const int ScreenWidth = 320;
        const int ScreenHeight = 240;
        static void Main(string[] args)
        {
            Pdg.Sdl2.System.Initialize(new SystemFlags() { Video = true }, new ImageFlags() { Png = true });
            using (var window = new Window("Splorr!! Dungeon Delver", null, null, ScreenWidth, ScreenHeight, new WindowFlags() { }))
            using (var renderer = new Renderer(window, null, new RendererFlags() { Accelerated = true }))
            using (var textScreen = new TextScreen(renderer, "cocofont.png"))
            {
                textScreen.ResetRenderer();
                MainMenu.Run(renderer, textScreen);
            }
            Pdg.Sdl2.System.Quit();
        }
    }
}
